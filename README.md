# README #

## What is this repository? ##

This repo provides easy to use translation files for all frontend projects, here you'll find `.po` files for each project.

### What is a `.po` file? ###

`.po` files are text files, they contain translations in a human-readable format, they are fiendly and easy to use and they can be viewed and edited on any text editor
or on specialized even frinedly`.po` files text editor like [Poedit](https://poedit.net)

All `.po` files have the same structure, there are several lines similar to the following:

```
msgid "This is the original text identificator of a text you are trying to translate"
msgstr "This is the text you want to translate"
```

this is how it looks on a text editor, but you won't have to edit the text there, because you are using a `.po` file editor like [Poedit](https://poedit.net) you'll see them organized in a better maner:

In two columns:

 - The first column is for the original text identifier, it helps to identify the text you want to edit, this row are *not editable*, it's the msgid, the message id)
 - The second column is for the text you want to translate, this rows are *editable*, it's the msgstr (message string)


### How is this repository organized? ###

There are 3 branches on this repository:

 - Master
 - Staging
 - Devel
 
 On every branch you'll find folders for each frontend project translation

 - Slides
 - Promotions
 - ...

 
Inside of each folder you'll find the translation file for that project, for each language you'll find its own `PO` file, for example, for Catalan there would be a `ca.po` file, for Spanish there would be a `es.po`, and so on.

### How do I find the translation file that I have to use? ###

For example, if you want to translate some Spanish text found on [promotions-dev.farmapremium.es/home](https://promotions-dev.farmapremium.es/home) you'll clone this repository and:

 - Go to the dev branch
 - Open the promotions folder
 - Edit the es.po file


### How do I identify the text I want to trasnlate? ###

Now that you've find the `.po` file and you've opened it with your favorite editor, [poedit](https://poedit.net), you'll have to search inside all the translatable strings the one that you want to translate:

There are two ways to find the string you want to translate, searching the string to tranlate, searching by the original identifier of the text you want to translate:

The *original text identifier* are weird strings like that:

*Home##header##button*

They may look ugly but they are here to help you, and me, organize the translations, the first words will be always capitalized, they identify the page you are on:

For example, if you want to translate the header of the chart found on [https://promotions-dev.farmapremium.es/home](https://promotions-dev.farmapremium.es/home) you'll look for:

*Home##chart##header*

If you want to translate a text found on  [https://slides.farmapremium.es/create/whatsapp](slides.farmapremium.es/create/whatsapp) you'll look for something like

*Create##Whatsapp##slogan*

Once you've found the key you want to modify, you'll find that they always show a default text in Spanish, this is to also help you to identify the string, and this is what you want to modify

```
msgid *Create##Whatsapp##form##slogan*
msgstr "This is the form slogan field"
```

You can also search the strings by the text you want to modify directly on the translated string column, this is fine, but you'll always have to check that the key related to this string is one the page you are expecting to find the text.

### i18n

We are using vue i18n *.json files for managing translations and typos.
To provide an user friendly interface that marketing teams can handle we are converting this *.json translations to *.po files.

We are using a node library called *i18next-conv* to do the json2po and po2json convertions

**Instalation**
```
npm install i18next-conv -g
```
**JSON2PO**
```
 i18next-conv -l es -s src/locales/es.json -t src/locales/es.po
```
**PO2JSON**
```
 i18next-conv -l es -s src/locales/es.po-t src/locales/es.json
```


